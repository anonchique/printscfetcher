namespace PrntScFetcher
{
    public interface ISettingsUpdater
    {
        void UpdateSettings(string lastUrl);
    }
}