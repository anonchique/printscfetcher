namespace PrntScFetcher
{
    public class Constants
    {
        public const string PICTURE_PATH = "pictures";
        
        public const string SETTINGS_PATH = "settings.txt";
        public const string USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36";
        public const string ERROR_SETTINGS_FILE_NOT_FOUND = "Файл отсутствовал, файл создан, но его необходимо заполнить!";
        public const string ERROR_WRONG_SETTINGS = "В настройках должно быть 6 латинских символов в нижнем регистре a-z, могут содержать цифры";
        public const string MAIN_URL = "https://prnt.sc/";
    }
}