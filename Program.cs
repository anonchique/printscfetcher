﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace PrntScFetcher
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var settings = new SettingsProvider();
            var startUrl = settings.GetStartUrl();

            var fetcher = new HttpFetcher(new Http(), new HtmlParser(), new UrlGenerator(Constants.MAIN_URL, startUrl), settings);

            while (true)
            {
                await fetcher.SaveImage();
                await Task.Delay(3000);
            }

        }


    }
}
