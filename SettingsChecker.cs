using System;
using System.IO;
using System.Text.RegularExpressions;

namespace PrntScFetcher
{
    public class SettingsProvider : ISettingsUpdater
    {
        public string GetStartUrl()
        {
            if (!Directory.Exists(Constants.PICTURE_PATH))
                Directory.CreateDirectory(Constants.PICTURE_PATH);

            if (!File.Exists(Constants.SETTINGS_PATH))
            {
                File.Create(Constants.SETTINGS_PATH);
                throw new Exception(Constants.ERROR_SETTINGS_FILE_NOT_FOUND + Environment.NewLine + Constants.ERROR_WRONG_SETTINGS);
            }

            var startFromUrl = File.ReadAllText(Constants.SETTINGS_PATH);
            startFromUrl = startFromUrl.ToLower();
            if (!ValidateStartUrl(startFromUrl))
                throw new Exception(Constants.ERROR_WRONG_SETTINGS);

            return startFromUrl;
        }

        public void UpdateSettings(string lastUrl) => File.WriteAllText(Constants.SETTINGS_PATH, lastUrl);
        private bool ValidateStartUrl(string startUrlTemplate) => Regex.IsMatch(startUrlTemplate, "^[a-z0-9]{6}$");
    }
}