using System.IO;
using System.Threading.Tasks;

namespace PrntScFetcher
{
    public class HttpFetcher
    {
        private readonly Http _http;
        private readonly HtmlParser _htmlParser;
        private readonly UrlGenerator _urlGenerator;
        private readonly ISettingsUpdater _settingsUpdater;


        public HttpFetcher(Http http, HtmlParser htmlParser, UrlGenerator urlGenerator, ISettingsUpdater settingsUpdater)
        {
            this._urlGenerator = urlGenerator;
            this._settingsUpdater = settingsUpdater;
            this._http = http;
            this._htmlParser = htmlParser;
        }

        public async Task SaveImage()
        {
            var html = await _http.GetHtml(_urlGenerator.Next());
            string picUrl = _htmlParser.GetPictureUrl(html);
            var resultBytes = await _http.GetPicBytes(picUrl);
            await File.WriteAllBytesAsync(Constants.PICTURE_PATH + $"\\{_urlGenerator.LastNumber}.png", resultBytes);
            _settingsUpdater.UpdateSettings(_urlGenerator.LastNumber);
        }

    }
}