using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrntScFetcher
{
    public class UrlGenerator
    {
        public string LastNumber { get; private set; }
        private string _staticUrl;
        private List<char> _letters = new List<char> { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
        public UrlGenerator(string staticUrl, string lastNumber)
        {

            this._staticUrl = staticUrl.Last() == '/' ? staticUrl : staticUrl + "/";
            this.LastNumber = lastNumber;
        }

        private string GetFullUrl() => _staticUrl + LastNumber;

        public string Next()
        {
            StringBuilder stringBuilder = new StringBuilder(LastNumber);
            bool isNewLoop = false;
            for (int i = LastNumber.Length - 1; i > 0; i--)
            {
                if (stringBuilder[i] == _letters.Last())
                {
                    stringBuilder[i] = _letters.First();
                    isNewLoop = true;
                    continue;
                }
                else
                {
                    if (isNewLoop)
                    {
                        var index = _letters.IndexOf(stringBuilder[i]);
                        stringBuilder[i] = _letters[index + 1];
                        LastNumber = stringBuilder.ToString();
                        return GetFullUrl();
                    }
                    else
                    {
                        var index = _letters.IndexOf(LastNumber.Last());
                        stringBuilder[stringBuilder.Length - 1] = _letters[index + 1];
                        LastNumber = stringBuilder.ToString();
                        return GetFullUrl();
                    }
                }
            }
            LastNumber = stringBuilder.ToString();
            return GetFullUrl();
        }
    }
}