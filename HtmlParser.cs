using System;
using System.Linq;
using HtmlAgilityPack;

namespace PrntScFetcher
{
    public class HtmlParser
    {
        internal string GetPictureUrl(string html)
        {
            HtmlDocument webSite = new HtmlDocument();
            webSite.LoadHtml(html);

            return webSite.DocumentNode.Descendants()
                    .Where(x => x.NodeType == HtmlNodeType.Element)
                    .Where(x => x.Attributes["property"]?.Value == "og:image")
                    .Select(x => x.Attributes["content"]?.Value)
                    .FirstOrDefault();
        }
    }
}