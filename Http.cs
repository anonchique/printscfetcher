using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Flurl.Http;
namespace PrntScFetcher
{
    public class Http
    {
        public async Task<string> GetHtml(string url)
        {
            var html = await url
                            .WithHeader("user-agent", Constants.USER_AGENT)
                            .GetStringAsync();
            return html;
        }

        /// <summary>
        /// Donwload bytes
        /// </summary>
        /// <param name="picUrl">Picture url</param>
        /// <returns></returns>
        internal async Task<byte[]> GetPicBytes(string picUrl)
        {
            var response = await new HttpClient().GetAsync(picUrl);
            return await response.Content.ReadAsByteArrayAsync();
        }
    }
}